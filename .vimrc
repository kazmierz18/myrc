set relativenumber
set showcmd


set smarttab
set expandtab
set tabstop=4
set shiftwidth=4

set ai " auto indent
set si " smart indent

" Use a line cursor within insert mode and a block cursor everywhere else.
"
" Reference chart of values:
"   Ps = 0  -> blinking block.
"   Ps = 1  -> blinking block (default).
"   Ps = 2  -> steady block.
"   Ps = 3  -> blinking underline.
"   Ps = 4  -> steady underline.
"   Ps = 5  -> blinking bar (xterm).
"   Ps = 6  -> steady bar (xterm).
let &t_SI = "\e[0 q"
let &t_EI = "\e[2 q"

set hlsearch

set nocompatible

syntax enable
filetype plugin on

set path+=**
set wildmenu
set wildignore=*.o,*~,*.pyc

set incsearch
set showmatch
map <silent> <space> :noh<cr>

set ruler

colorscheme desert

let g:netrw_liststyle=3 " tree view
let g:netrw_altv=1      " open tree to the right

set noerrorbells
set novisualbell
set vb t_vb=
set tm=500
set belloff=all

set so=10
set ignorecase

"augroup netrw_mapping
    "autocmd!
    "autocmd filetype netrw call NetrwMapping()
"augroup END
"
"function! NetrwMapping()
    "noremap <buffer> l <CR>
    "noremap <buffer> h -
"endfunction
